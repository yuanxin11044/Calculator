FROM openjdk:8
ADD target/calculator.jar calculator.jar
ENTRYPOINT ["java", "-jar","calculator.jar"]
